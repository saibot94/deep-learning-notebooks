
# Pre-process the images and add the labels for them
IMG_SIZE = 32
NUM_CLASSES=62
ROOT_PATH = 'C:/Users/cristian.schuszter/Desktop'

root_training_dir = ROOT_PATH + '/Traffic sign recognition dataset/Training'
root_testing_dir = ROOT_PATH + '/Traffic sign recognition dataset/Testing'

def preprocess_image(img):
    """
    Take an image from the initial ppm dataset and then preprocess it so that it looks good.
    Then, resize to some standard shape provided above
    """
    hsv = color.rgb2hsv(img)
    hsv[:, :, 2] = exposure.equalize_hist(hsv[:, :, 2])
    img = color.hsv2rgb(hsv)
    
    min_side = min(img.shape[:-1])
    centre = img.shape[0] // 2, img.shape[1] // 2
    img = img[centre[0] - min_side // 2 : centre[0] + min_side // 2,
             centre[1] - min_side // 2 : centre[1] + min_side // 2]
    img = transform.resize(img, (IMG_SIZE, IMG_SIZE), mode = 'reflect')
    #img = np.rollaxis(img, -1)
    return img


def get_class(path):
    cls = int(path.split('\\')[-2])
    return cls

def get_tsr_train_data():
    all_image_paths = glob.glob(os.path.join(root_training_dir, '*/*.ppm'))
    imgs = []
    labels = []
    count = 0
    np.random.shuffle(all_image_paths)
    for img_path in all_image_paths:
        img = preprocess_image(io.imread(img_path))
        label = get_class(img_path)
        labels.append(label)
        if count % 20 == 0:
            print('{:.2f}% of all training images processed'.format((count / len(all_image_paths) * 100)))
        count+=1
        imgs.append(img)
    print ('Done processing training images!')
    return imgs, labels
    

def get_tsr_test_data():
    all_image_paths = glob.glob(os.path.join(root_testing_dir, '*/*.ppm'))
    imgs = []
    labels = []
    count = 0
    np.random.shuffle(all_image_paths)
    for img_path in all_image_paths:
        img = preprocess_image(io.imread(img_path))
        label = get_class(img_path)
        labels.append(label)
        if count % 20 == 0:
            print('{:.2f}% of all testing images processed'.format((count / len(all_image_paths) * 100)))
        count+=1
        imgs.append(img)
    print ('Done processing test images!')
    return imgs, labels